Unit B: Clouds and Climate Change
===============================================

How to approach this material
-----------------------------------------------
Each unit includes an online outline, which is matched by a powerpoint file with classroom lecture slides and data activities. A teacher can follow the outline to discover linked resources, or can browse all available resources, which are organized by unit.  

The editable powerpoint can be downloaded `here <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Powerpoints/Clouds_and_Climate.pptx>`_ (warning: large 1.5G file).

A pdf of the powerpoint, sized for easy download and sharing is also available `here <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Powerpoints/Clouds_and_Climate.pdf>`_.

Each of the units can be used independently from one another, sequentially or not. It all depends on preference in how to introduce the topic at hand and what tool is deemed most appropriate. 

The main Power Point dives right into the science in an interdisciplinary fashion. This tool is designed to help demystify the content and to enhance understanding. 

The Data Analysis Tool is designed to engage students in learning how to use Excel to analyze and represent scientific data used by scientists to study climate change phenomena. The data tools have step-by-step instructions on how to access data and process it. It also helps teach how to perform statistical analysis to render the data useful. 

The third tool is a Case Study that helps introduce the impact these climatological changes are having on people and ecosystems. Due to topic overlap, please see the Sahel Case Study located in Unit A, there is no separate case study for Unit B.  The Sahel case study students also have the opportunity to learn about sustainable solutions by engaging in a engineering design project. 

Each Power Point has robust notes on most slides that serve as a lesson plan of sorts. All the pertinent information and downloadable teaching tools are also organized in a sequential fashion in the note section of the slides. The Power Points have downloaded videos to avoid lag time, access and streaming problems. In the note sections there are also the links to all related images and downloaded documents in case needed.
Each tool includes:

* Student assignments in the from of handouts, 
* Lab activities with step by step instructions and analysis questions; 
* Video transcripts when appropriate;
* Suggested Reading assignments in PDF format

Introduction
~~~~~~~~~~~~~~~~~~

Clouds are an integral part of the Earth’s climate system. They help modulate the Earth’s radiation budget, and transport vast amounts of water in the form of precipitation across the globe. Clouds have a dual role in helping regulate the Earth’s temperature; they have a warming and cooling effect. This is determined by their chemistry and the altitude in which they reside. Their chemistry is influenced by tiny liquid or solid suspended particles called aerosols that serve as substrates for condensation to take place. The chemical properties of aerosols help determine the size of the cloud water droplet affecting the clouds reflectivity and precipitation capacity. Anthropogenic aerosols can promote the formation of smaller cloud droplets rendering the cloud brighter forcing increasing its reflectivity. Therefore blocking solar rays from reaching the Earth’s surface having a direct impact on the Earth’s Radiation Budget. Smaller droplets may never grow big enough to fall out of clouds, suppressing precipitation.  

Understanding cloud development, their chemistry, thermal and reflectivity capacity are key to forecasting climate change. Clouds and climate are part of a complicated feedback system driven by energy. If one of these systems changes then the other is inevitably affected. Clouds affect climate and climate in turn affect clouds. In order to successfully model climate change and variability; global cloud observational data on how they form and travel; their precipitation patterns; energy capacity and chemical interactions among other measurements are essential.
Climate data of these variable physical parameters serves to inform computational climate models in order to study changing conditions in order to provide accurate forecasts to help humanity prepare and mitigate global warming, therefore climate changes.

Section I. Clouds: What are They?
--------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~~

Introduce basic concepts about clouds; elicit students conceptions about clouds; and how clouds and climate are intimately related. 

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Introduce the following concepts:
* Clouds have a dual role: helping to keep our planet warm; and their cooling effect by also shielding us from solar radiation.
* Clouds are an integral part of the climate system driven by energy. 
* Clouds and Climate are part of a complicated feedback system, if one is affected then the other one two. 
* Elicit from the students their understanding of what clouds are made of.

Section II. Cloud Classification and Characteristics
------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~

Introduce the physical properties of clouds, their respective characteristics and types.

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Introduce and learn in detail about:

* Cloud classification: how their respective names are based on their shapes and altitude.
* Identify clouds by names and properties. 
* How their altitude defines if they are made of water, water and ice crystals, or ice crystals.
* What type of precipitation is associated with the cloud type.
* How their development and position in the atmosphere is dependent on the energy and moisture availability. 

Suggested Readings
~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Ten Basic Cloud Types (NOAA, National Weather Service), `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S2_Clouds_Readings/S2_Ten_Basic_Cloud_Types.pdf>`_,  `Online link <http://www.srh.noaa.gov/jetstream/clouds/cloudwise/types.html>`_ 

* Cloud Chart (NASA), `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S2_Clouds_Readings/S2_NOAA_NASA_CloudChart.pdf>`_,  `Online link <https://science-edu.larc.nasa.gov/cloud_chart/>`_

Section III. Cloud Formation
------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Learn how clouds are formed depending on suspended particles in the atmosphere and how much energy and moisture is available. 

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Learn about: 

* How clouds are a product evaporation, evapotranspiration, condensation and nucleation. 
* Physical and chemical reactions involving evaporation and condensation.
* Environmental heat exchanges for the evaporative process and condensation to happen.
* The mechanisms that fuel the rise of water vapor, such as convection, convergence and lift.
* Introduction to fundamental concepts of gas laws: temperature, density and pressure. 
* The mechanisms involved in the condensation of water vapor in the atmosphere.
* How water droplets are formed by attaching to particles suspended in the atmosphere that serve as substrates and condensation sites.
* What are condensation nuclei and their function.
* What is the volume a rain droplet needs to reach in order to precipitate out of a cloud.

Lab Activities
~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

* Lab - The Physical and Chemical Properties of Phase Change - Introduction to Thermodynamics  - Slide 14, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Activities_and_Labs/S3_Clouds_Activities_and_Labs/S3_Dry_Ice_Lab.pdf>`_
* Lab - Heat Rises - Slide 18, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Activities_and_Labs/S3_Clouds_Activities_and_Labs/S3_Heat_Rises.pdf>`_
* Lab - Solar Bag Laboratory - Slide 18, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Activities_and_Labs/S3_Clouds_Activities_and_Labs/S3_Solar_Bag_Experiment_Lab.pdf>`_
* Lab - Finding Relative Humidity - Slide 19, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Activities_and_Labs/S3_Clouds_Activities_and_Labs/S3_Finding_Relative_Humidity.pdf>`_
* Lab - Homemade Psychrometer - Slide 19, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Activities_and_Labs/S3_Clouds_Activities_and_Labs/S3_Homemade_Psychrometer_2.pdf>`_
* Lab - Gas Convection Chamber - Slide 20 and 21, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Activities_and_Labs/S3_Clouds_Activities_and_Labs/S3_Gas_Convection_Chamber_Lab.pdf>`_
* Lab - Condensation - Making a Cloud - Slide 31, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Activities_and_Labs/S3_Clouds_Activities_and_Labs/S3_Cloud-in-a-Bottle%202.pdf>`_
* Lab - Cloud Droplet and Rain Droplet  - Slide 34, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Activities_and_Labs/S3_Clouds_Activities_and_Labs/S3_Clouds_Droplets_and_Rain_Drops.pdf>`_

Suggested Readings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

* Evapotranspiration: The Water Cycle (USGS), Slide 16, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S3_Clouds_Readings/S3_Evapotranspiration_The_Water_Cycle_USGS.pdf>`_,  `Online link <https://water.usgs.gov/edu/watercycleevapotranspiration.html>`_

* Condensation: The Water Cycle (USGS), – Slide 33, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S3_Clouds_Readings/S3_Condensation_The_Water_Cycle_USGS.pdf>`_,  `Online link <https://water.usgs.gov/edu/watercyclecondensation.html>`_

* Conditions for Cloud Formation (University of Arizona, Department of Atmospheric Sciences), – Slide 22, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S3_Clouds_Readings/S3_All_About_Clouds_How_Clouds_are_Formed.pdf>`_,  `Online link <http://www.atmo.arizona.edu/students/courselinks/fall12/atmo336/lectures/sec1/formation.html>`_

* Changing Cloud Cloudiness (NASA), `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S3_Clouds_Readings/S3_Changing_Global-Cloudiness.pdf>`_,  `Online link <Link: https://earthobservatory.nasa.gov/Features/GlobalClouds/>`_

* All About Clouds: How Clouds are Formed (NOAA), Slide 29 - 31, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S3_Clouds_Readings/S3_All_About_Clouds_How_Clouds_are_Formed.pdf>`_,  `Online link <http://www.srh.noaa.gov/jetstream/clouds/formation.html>`_

* How Clouds are Formed: Changing Global Cloudiness (NASA), Slide 30, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S3_Clouds_Readings/S3_How_do_Clouds_Form_Changing_Global_Cloudiness.pdf>`_,  `Online link <https://earthobservatory.nasa.gov/Features/GlobalClouds/>`_

Section IV. Precipitation: One of Two Primary Cloud Functions
-----------------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Learn how clouds generate precipitation and how they can be affected by pollution of aerosol particles.

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Learn about:

* The effect of the density of CCNs (condensation cloud nuclei) on cloud formation due to pollution of anthropogenic aerosols, and how might this affect cloud precipitation. 
* How anthropogenic aerosols affect the size of cloud water droplet.
* Global precipitation distribution and its relation to energy availability.
* How the development of rain cloud systems can be affected by aerosol pollution.

Lab Activities
~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Lab - Water, Water, Everywhere, Slide 40, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Activities_and_Labs/S4_Clouds_Activities_and_Labs/S4_Water_Water_Everywhere_NOAA.pdf>`_ 

* Lab - Legend for Color Coded Precipitation Map, Slide 43, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Activities_and_Labs/S4_Clouds_Activities_and_Labs/S4_Legend_for_Color_Coded_Precipitation_Map.pdf>`_  

* Lab - Graph of Trapped Energy and Water Vapor, Slide 44, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Activities_and_Labs/S4_Clouds_Activities_and_Labs/S4_Graph_of_Trapped_Energy_and_Water_Vapor.pdf>`_

Suggested Readings
~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Aerosols and Clouds (NASA), Slide 39, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S4_Clouds_Readings/S4_Aerosols_and_Clouds_Aerosols_Tiny_Particles_Big_Impact.pdf>`_,  `Online link <https://earthobservatory.nasa.gov/Features/Aerosols/page4.php>`_

* The Hydrologic Cycle (NOAA), Slide 40, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S4_Clouds_Readings/S4_The_Hydrologic_Cycle_NOAA-NWS_JetStream.pdf>`_,  `Online link <http://www.srh.noaa.gov/jetstream/atmos/hydro.html>`_

* Water Vapor Global Maps (Earth Observatory, NASA), Slide 44, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S4_Clouds_Readings/S4_Water_Vapor_Global_Maps.pdf>`_  `Online link 1 <https://earthobservatory.nasa.gov/GlobalMaps/view.php?d1=MYDAL2_M_SKY_WV>`_, `Online link 2 <https://www.nasa.gov/topics/earth/features/vapor_warming.html>`_

* Water Cycle Overview (NASA Precipitation Education), `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S4_Clouds_Readings/S4_Water_Cycle_Overview.pdf>`_,  `Online link <https://pmm.nasa.gov/education/articles/earth-observatory-water-cycle-overview>`_

* Earth’s Water Cycle – Transcript (NASA), Slide 47, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S4_Clouds_Readings/S4_Earths_Water_Cycle_Transcript.pdf>`_,  `Online link <https://pmm.nasa.gov/education/videos/earths-water-cycle>`_

Section V. Clouds and The Earth’s Radiation Budget
------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Learn about the role of clouds in the Earth’s Radiation Budget. 

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Learn about:

* What is the Earth’s Radiation Budget and how it works.
* How the radiation budget is affected by clouds.
* How clouds cool the Earth’s surface by reflecting incoming shortwave solar radiation.
* Visible Light and UV solar rays.
* How clouds warm the Earth’s Surface by absorbing longwave Infra-Red thermal radiation and emitting it back to earth.
* How different types  of clouds interact with different types of radiation. 

Lab Activities
~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Earth’s Radiation Budget Worksheet – Slide 51, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Activities_and_Labs/S5_Clouds_Activities_and_Labs/S5_Earth_Radiation_Budget_Chart_Worksheet.pdf>`_

* ARIS Total Precipitable Water Vapor Map – Slide 59, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Activities_and_Labs/S5_Clouds_Activities_and_Labs/S5_ARIS_Global_Total_Precipitable_Water_Vapor_for_May_2009.pdf>`_ 

* World Geographic Reference System Map – Slide 60, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Activities_and_Labs/S5_Clouds_Activities_and_Labs/S5_Global_Precipitable_Water_Vapor_to_Rain.pdf>`_

Suggested Readings
~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Electromagnetic Spectrum, Booklet Guide to Understanding Radiation, Slide 49, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S5_Clouds_Readings/S5_Electromagnetic_Spectrum.pdf>`_,  `Online link <https://smd-prod.s3.amazonaws.com/science-green/s3fs-public/atoms/files/Tour-of-the-EMS-TAGGED-v7.pdf>`_

* What is the Earth’s Radiation Budget? (NASA), Slide 52 - 57, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S5_Clouds_Readings/S5_What_Is_The_Earths_Radiation_Budget.pdf>`_,  `Online link <https://science-edu.larc.nasa.gov/EDDOCS/whatis.html>`_

* Climate and Earth’s Energy Budget (NASA), Slide 55 - 56, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S5_Clouds_Readings/S5_Climate_and_Earths_Energy_Budget.pdf>`_,  `Online link <https://earthobservatory.nasa.gov/Features/EnergyBalance/printall.php>`_

* Global Total Precipitable Water Vapor for May 2009 (Jet Propulsion Laboratory, California Institute of Technology), Slide 59, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S5_Clouds_Readings/S5_Global_Total_Precipitable_Water_Vapor_for_May_2009.pdf>`_,  `Online link <https://www.jpl.nasa.gov/spaceimages/details.php?id=PIA12096>`_

* Cloud Forcing: Radiation and Clouds (Earth Observatory, NASA), Slide 61, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S5_Clouds_Readings/S5_Cloud_Forcing_Clouds_and_Radiation.pdf>`_,  `Online link <https://earthobservatory.nasa.gov/Features/Clouds/clouds2.php>`_
	
* High Clouds: Radiation and Clouds (Earth Observatory, NASA), Slide 62, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S5_Clouds_Readings/S5_High_Clouds_Clouds_Radiation.pdf>`_  `Online link <https://earthobservatory.nasa.gov/Features/Clouds/clouds3.php>`_

* Low Clouds: Clouds and Radiation (Earth Observatory, NASA), Slide 63, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S5_Clouds_Readings/S5_Low_Clouds_Clouds_and_Radiation.pdf>`_  `Online link <https://earthobservatory.nasa.gov/Features/Clouds/clouds4.php>`_

* Deep Convective Clouds: Clouds and Radiation (NASA), `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S5_Clouds_Readings/S5_Deep_Convective_Clouds_and_Radiation.pdf>`_, `Online link <https://earthobservatory.nasa.gov/Features/Clouds/clouds5.php>`_

Section VI. Clouds Influence on Climate Change
------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Learn how cloud pollution can interact with clouds and suppress precipitation and negatively impact climate.

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Learn how:

* Aerosol pollution in clouds can have an impact on the Earth’s Radiation Budget.
* Sulfate aerosols interact with clouds and affect their reflectivity.
* Ship exhaust affects the formation of clouds and promotes radiative forcing.
* Scientists record and analyze data to discover the effects pollution have on clouds.
* Contamination of clouds can suppress precipitation.
* Scientists and Engineers combined develop advanced technology that allow climatologist to study clouds physiology, precipitation, raindrop size and the relation between water droplet and energy available in storm clouds.

Lab Activity
~~~~~~~~~~~~~~~~~~~~~~~~~~~
Radiation and Color Lab - Slide 67, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Activities_and_Labs/S6_Clouds_Activities_and_Labs/S6_Radiation_and_Color_Lab.pdf>`_ 

Suggested Readings
~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Aerosols and Clouds (Earth Observatory, NASA), Slide 67, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S6_Clouds_Readings/S6_Aerosols_and_Clouds_Tiny_Particles_Big_Impact.pdf>`_, `Online link <https://earthobservatory.nasa.gov/Features/Aerosols/page4.php>`_

* Pollution and Clouds (Rosenfeld and Woodley), Slide 68 - 71, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S6_Clouds_Readings/S6_Pollution_and_Clouds_PWFEB01rosenfeld.pdf>`_, `Online link <http://www.meto.umd.edu/~zli/METO401/AOSC401/Additional%20Readings/Aerosol%20papers/PWFEB01rosenfeld.pdf>`_

* Indirect Effects of Aerosols on Clouds and Precipitation (Intergovernmental Panel of Climate Change), Slide 71, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S6_Clouds_Readings/S6_Indirect_Effects_of_Aerosols_on_Clouds_and_Precipitation.pdf>`_, `Online link <https://www.ipcc.ch/publications_and_data/ar4/wg1/en/ch7s7-5-2.html>`_

* Changing our Weather one Smokestack at a Time, Studying Ship Tracks (Earth Observatory, NASA), `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S6_Clouds_Readings/S6_Changing_Our_Weather_One_Smokestack_at_a_Time.pdf>`_, `Online link <https://earthobservatory.nasa.gov/Features/Pollution/pollution_2.php>`_

* Measuring Raindrop Sizes From Space Video Transcript (NASA), Slide 74, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Readings/S6_Clouds_Readings/S6_Measuring_Raindrop_Sizes_From_Space_to_Understand_Storms_Transcript.pdf>`_, `Online link <https://www.nasa.gov/feature/goddard/2016/size-matters-nasa-measures-raindrop-sizes-from-space-to-understand-storms>`_