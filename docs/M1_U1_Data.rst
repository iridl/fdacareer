Unit A: Data Analysis and Visualization, Atmospheric Aerosols and Climate Change 
==================================================================================

How To Use This Data Unit
-----------------------------------------------

Students will use three data sets in the form of Excel spreadsheets that will allow them to piece together information that will help them understand how the global climate system can affect a specific region of the world due to anthropogenic pollution. 

The editable powerpoint can be downloaded `here <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/Aerosol_Data_Powerpoints/Aersols_and_Aerosol_DATA_Unit.pptx>`_.

A pdf of the powerpoint is also available `here <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/Aerosol_Data_Powerpoints/Aersols_and_Aerosol_DATA_Unit.pdf>`_.

Introduction
-----------------------------------------------

The African Sahel Region has suffered a prolonged drought that lasted 3 decades spanning from the late 1960’s, 1970’s to the 1980’s. During this time period the Sahel had a 30% decline in rainfall, only recently has it experienced a partial recovery. The Sahelian drought is the most severe drought of the 20th century. This severe drought adversely impacted 100 million people that depend of this precious resource for their subsistence. 

The changes in climate that are afflicting the Sahel is of global concern. Scientists from across the world began to study this phenomenon and the broader implications with respect to the health of our global climate. According to the latest climate models supported by recorded data the cause of this prolonged drought is the decrease in surface temperature of the North Atlantic Ocean due to an atmospheric phenomena called global dimming. This occurs when highly reflective air pollution particles suspended in the troposphere and in clouds impede the shortwave radiation from the sun to reach the surface of the ocean. The pollutant that has directly been tied to this effect is Sulfur Dioxide (SO\ :sub:`2`\ ). 

The cooling effect of SO\ :sub:`2`\  was observed and recorded when Mt. Pinatubo and Mt. Hudson (1991) ejected 23 million tons of SO\ :sub:`2`\  into the atmosphere circling the globe. The global average temperature decreased by 0.6 degrees C for two years with a gradual recovery. This data confirmed previous theories that SO\ :sub:`2`\  aerosol pollution affects climate. Other recorded observations have confirmed this effect as well. The cooling effect of SO\ :sub:`2`\  particle emissions from ship exhaust produce clouds that have a high density of polluted smaller than average water droplets that increase the clouds reflectivity contributing to the dimming effect. This evidence help narrow the causes of the cooling of certain regional surfaces when the global trend was of warming temperatures.

During the 60’s, 70’s and 80’s the U.S. and Europe power plants and industry injected the atmosphere with thousands of megatons of SO\ :sub:`2`\  annually, making them the largest polluters. Data revealed that during that time surface temperatures of the North Atlantic Ocean decreased its’ average temperature by three tenth’s of a degree Celsius (0.3°C) compared to its’ historical average. Although this may seem insignificant these subtle changes can drastically impact regional climates. The laws of thermodynamics predicate that there is a correlation between available thermal energy and precipitation. Lower sea surface temperatures mean a reduction on evaporative rates reducing the amount of moisture (water vapor) available in the atmosphere to precipitate over land. Conversely, the opposite is true – higher sea surface temperatures cause higher evaporation rates, which produce more moisture hence higher precipitation. 

The cooling and warming of the ocean surface can impact how much rain falls on a geographical region. A few tenths of a degree temperature change up or down over large geographical areas causes fluctuations in precipitation. The sea-surface temperature (SST) has an effect on the fluctuations of heat and moisture into the atmosphere. The cooling of the North Atlantic Ocean reduced the moisture available to produce the copious amounts of precipitation the Sahel was accustomed to during the Monsoon rain season during the drought. The Sahel is located north of the tropical rain belt known as the Inter Tropical Convergence Zone that lies 6° north of the equator. The ITCZ shifts naturally north during the summer season and south during the winter season. This system is responsible for bringing rain to the Sahel. The cooling of the North Atlantic Ocean affects the up and down motion of the ITCZ. During the drought the ITCZ shifted further south of what it’s normal latitude location is during the summer months and deprived the Sahel of the much-needed rain. 

The U.S. and the European Union have since significantly reduced SO\ :sub:`2`\  emissions in an effort to clean the air due to health hazards and acid rain. This has had a positive effect. As the SO\ :sub:`2`\  levels where dramatically reduced through use of engineered technologies there have been signs of improvement of increased precipitation in the Sahel.  

Data Section I. Global Dimming and Sulfur Dioxide
------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Introduce students to the concept of global dimming, its causes, and how it affects climate.

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Describe what is and what causes global dimming.
Gain an understanding of the physical and chemical properties of Sulfur Dioxide.
List some of the natural and anthropogenic sources of SO\ :sub:`2`\  emissions.
Understand the mechanisms in which SO\ :sub:`2`\  reacts with water to form sulfuric acid.
Understand the distinction between natural sources and anthropogenic sources of SO\ :sub:`2`\ .
Understand the atmospheric SO\ :sub:`2`\  cycle. 
Describe the reflective properties of SO\ :sub:`2`\  in the atmosphere and how it serves as a condensation nuclei in the cloud formation process.
Describe how SO\ :sub:`2`\  has a cooling effect and how it affects the Earth’s Energy Budget.

Suggested Readings 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Aerosols and Climate: Oxford Research Encyclopedia on Climate Science (Oxford Research Encyclopedias ), Slide 2, `Online Link <http://climatescience.oxfordre.com/view/10.1093/acrefore/9780190228620.001.0001/acrefore-9780190228620-e-13?print>`_

* Aerosols and Incoming Sunlight (NASA), Slide 6, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S1_Aerosol_DATA_Readings/S1_Aerosols_and_Incoming_Sunlight.pdf>`_, `Online Link <https://earthobservatory.nasa.gov/Features/Aerosols/page3.php>`_

Data Section II. Sulfur Dioxide Emission Sources
------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Introduce students to the types of industries responsible for Sulfur Dioxide anthropogenic emissions in US and Europe that are responsible for global dimming.

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

List the type of industries that emit Sulfur Dioxide in the US and Europe.
Understand the history of increase of SO\ :sub:`2`\  levels.
List which industries and countries have the highest emissions of Sulfur Dioxide.

Suggested Readings/Lab Activity
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Challenges in Air Pollutants in the United States (Georgia State University, Climate Literacy), Slide 8, `Online Link <http://sites.gsu.edu/geog1112/lab-3-day-2-part-3-2/>`_ 

Data Section III. SO\ :sub:`2`\  Effects on Climate
------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Expose students to scientific evidence that SO\ :sub:`2`\  emissions cause global dimming and regional cooling on mean temperatures.

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Explain the scientific evidence that confirms SO\ :sub:`2`\  emissions cause global dimming and regional cooling of mean temperatures.
Import a csv format file in excel to generate a time series graph of land and sea global mean data.
Analyze the time series graph and explain the significance of the data.

Suggested Readings 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Aerosols and Climate Oxford Research Encyclopedia of Climate Science (Oxford Research Encyclopedias ) PDF – Slide 13, `Online Link <http://climatescience.oxfordre.com/view/10.1093/acrefore/9780190228620.001.0001/acrefore-9780190228620-e-13?print>`_ 

* Observed Reductions of Surface Solar Radiation in US and Worldwide (Geophysical Research Letters), Slide 19, `Online Link <http://onlinelibrary.wiley.com/doi/10.1029/2002GL014910/full#grl15787-fig-0001>`_ 

Lab Activities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Global Land and Ocean Temperature Anomalies Data Analysis, Slide 15, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S3_Aerosols_Data/S3_Global_Land_and_Ocean_Temperture_Anomalies_Data_Analysis.pdf>`_

Data Files (Excel)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* `Global Land and Ocean Temperature Anomalies 1880 – 2016 Baseline 1910 – 2000 <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S3_Aerosols_Data/S3_Global_Land_and_Ocean_Temperature_Anomalies_1880_2016_Baseline_1910_2000.xlsx>`_
* `Global Land and Ocean Temperature Anomalies Student <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S3_Aerosols_Data/S3_Global_Land_and_Ocean_Temperature_Anomalies_Student.xlsx>`_
* `Global Land and Ocean Temperature Anomalies 1880 – 2016 Teacher <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S3_Aerosols_Data/S3_Global_Land_and_Ocean_Temperature_Anomalies_1880_2016_Baseline_1910_2000_Teacher.xlsx>`_

Data Section IV.  Impact of SO\ :sub:`2`\  Global Dimming Affected the ITCZ and Precipitation in The Sahel
----------------------------------------------------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Introduce students to the African Monsoon climate; how it’s regulated by the Inter Tropical Convergence Zone; and how the region is susceptible to ITCZ shifts and variability impacting the regions precipitation levels. 

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Define what is the Inter Tropical Convergence Zone (ITCZ).
Describe seasonal ITCZ shifts and variability due to seasonal and changes.
Analyze time series precipitation graph for the Sahel to gain a understanding of the precipitation and drought history of the Sahel.

Suggested Readings  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* The West African Monsoon (National Taiwan Normal University), Slide 21, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S4_Aerosol_DATA/S4_The_West_African_Monsoon.pdf>`_, `Online link <http://www1.geo.ntnu.edu.tw/webs/teacher/Shu-Ping%20Weng/course/monsoon%20winds/Lecture10/08%20The%20west%20African%20monsoon.pdf>`_

* Sea Surface Temperature and Water Vapor (NASA), Slide 23, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S4_Aerosol_DATA/S4_Sea_Surface_Temperature_Water_Vapor.pdf>`_, `Online link <https://earthobservatory.nasa.gov/GlobalMaps/view.php?d1=MYD28M&d2=MYDAL2_M_SKY_WV>`_

* Sahel Temporary or Permanent Desert (Earth Observatory, NASA), Slide 24, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S4_Aerosol_DATA/S4_Sahel_Temporary_Drought_Permanent_Desert.pdf>`_, `Online link <https://earthobservatory.nasa.gov/Features/Desertification/desertification2.php>`_

* Precipitation in Africa and the ITCZ, or Inter Tropical Convergence Zone (Penn State University, College of Earth and Mineral Science), Slide 25, `Online link <https://courseware.e-education.psu.edu/courses/earth105new/content/lesson07/03.html>`_

* Dynamical Outlines of the Rainfall Variability and the ITCZ Role over the West Sahel (Atmospheric and Climate Sciences), Slide 26, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S4_Aerosol_DATA/S4_Dynamical_Outlines_of_the_Rainfall_Variability_and_the_ITCZ_Role_over_the_West_Sahel.pdf>`_, `Online link <http://file.scirp.org/Html/7-4700058_21400.htm>`_

Lab Activities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Rain Belt (ITCZ) Inter Tropical Convergence Zone Motion Patterns, Slide 22, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S4_Aerosol_DATA/S4_Rain_Belt_ITCZ_Motion_Pattern.pdf>`_  
* Rain Belt (ITCZ) Inter Tropical Convergence Zone Motion Shifts, Slide 25, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S4_Aerosol_DATA/S4_Rain_Belt_ITCZ_Inter_Tropical_Convergence_Zone_Motion_Shifts.pdf>`_  
* Precipitation Anomalies Sahel, Slide 26, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S4_Aerosol_DATA/S4_Sahel_Precipitation_Anomalies_Sahel.pdf>`_ 

Data Section V.  SO\ :sub:`2`\  and Northern Atlantic Sea Surface Temperature Data Analysis
------------------------------------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Use climate and pollution emissions data from various sources to develop and analyze various time series graphs to assist in the understanding of the correlation between SO\ :sub:`2`\  emissions and decrease in sea surface temperatures (SST) that caused a significant reduction in the amount of rainfall in the Sahel Region of Africa.
Learn how climate models are developed and used to help identify causality of climate forcings and predict future climate changes influencing precipitation in the Sahel.

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Import csv. data files proficiently in Excel and develop a spreadsheet and data graphs.
Calculate the monthly and annual means for different types of data using Excel.
Generate line and scattered graphs using Excel.
Insert and analyze graph trendlines. 
Analyze and compare data graphs to infer the cause of changes in the precipitation levels in the Sahel for the past four decades. 
Explain how computer simulations models are used to help study the problem and forecast precipitation patterns in the Sahel.
Explain how pollution regulation in United States and Europe have had an effect on levels of air pollution.
Explain the correlation between levels of SO\ :sub:`2`\  Pollution in U.S. and Europe affected the climate of the Sahel.

Suggested Readings   
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Asthma and Air Pollution in the South Bronx (Institute for Civil Infrastructure Systems), Slide 30, `Online Link <http://www.icisnyu.org/south_bronx/AsthmaandAirPollution.html>`_, `Online link for additional sites in the vicinity <http://www.icisnyu.org/south_bronx/AirQuality.html>`_

* Why do Scientists Measure Sea Surface Temperature? (NOAA National Ocean Service), Slide 44, `Online link <https://oceanservice.noaa.gov/facts/sea-surface-temperature.html>`_

* Scatterplots and Regression Trendlines (Purplemath.com),Slide 44 - 45, `Online link <http://www.purplemath.com/modules/scattreg2.htm>`_

* A Unifying View of Climate Change in the Sahel linking Intra-seasonal, Intrannual and Longer Time Scales (Alessandra Giannini, S Salack, T Lodoun, A T Gaye and O Ndiaye), Slide 46, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_Giannini_2013_ClimateChange_Sahel.pdf>`_, `Online link <http://iopscience.iop.org/article/10.1088/1748-9326/8/2/024010/meta>`_

* Introducing Basic Drivers of Climate (The Nature Education, Project Knowledge), Slide 50, `Online link <https://www.nature.com/scitable/knowledge/library/introduction-to-the-basic-drivers-of-climate-13368032>`_

* Paleoclimatology: Introduction (Earth Observatory, NASA), Side 50, `Online link <https://earthobservatory.nasa.gov/Features/Paleoclimatology/paleoclimatology_intro.php>`_

* Climate Forcing (Open Source, System, Science Solutions), Slide 52, `Online link <http://ossfoundation.us/projects/environment/global-warming/radiative-climate-forcing>`_

* How do scientist measure global temperatures? (Carbonbrief.org), Slide 54, `Online link <https://www.carbonbrief.org/explainer-how-do-scientists-measure-global-temperature>`_

* Report: IPCC AR5 Chapter 10, (Intergovernmental Panel on Climate Change), `Online link <http://www.ipcc.ch/pdf/assessment-report/ar5/wg1/WG1AR5_Chapter10_FINAL.pdf>`_

* Summary of the Clean Air Act Law 42 U.S.C. § 470 et. Seq., 1970 (EPA), `Online link <https://www.epa.gov/laws-regulations/summary-clean-air-act>`_ 

Lab Activities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Graph and Analyze Bronx SO\ :sub:`2`\  Emissions, Slide 31 - 36, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_Graph_and_Analyze_Bronx_SO_Emissions%20.pdf>`_
* National SO\ :sub:`2`\  Emissions Trend, Slide 41 - 43, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_National_SO2_Emissions_Trend.pdf>`_
* Video Transcript Simulation Findings, Slide 46 - 47 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_Video_Transcript_Giannini_Explain_Computer_Simulation_Findings.pdf>`_
* Northern Atlantic Relative Index Sea Surface Temperature Anomalies, Slide 37 `Word Document <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_North_Atlantic_Relative_Index%20Anomaly_Graph_and_Analysis.docx>`_
* Sahel Precipitation Anomalies Data Analysis, Slide 38, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_Precipitation_Anomalies_in_the_Sahel.pdf>`_
* Global Mean Surface Temperature Simulation Modeling, Slide 54, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_Global_Mean_Surface_Temperature_Simulation_Modeling_Student.pdf>`_
* Sahel Precipitation Computer Simulation Model Graph Analysis,  Slide 56 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_Sahel_Precipitation_Computer_Simulation_Model_Graph_Analysis.pdf>`_

Data Files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* `SO2  Data Aerosols Bronx Teacher.xlsx <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_SO2_DATA_Aerosols_BRONX_Teacher.xlsx>`_ 
* `SO2 Data Aerosols Bronx Student.xlsx <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_SO2_DATA_Aerosols_BRONX_Student.xlsx>`_
* `Sulfur Dioxide National.csv <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_Sulfur_Dioxide_National.csv>`_
* `Sulfur Dioxide National Teader.xlsx <hhttps://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_Sulfur_Dioxide_National_Teacher.xlsx>`_
* `North Atlantic Relative Index Sea Surface Temperature Teacher.xlsx <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_North_Atlantic_Relative_Index_Sea_Surface_Temperature_Teacher.xlsx>`_
* `North Atlantic Relative Index Sea Surface Temperature Student.xlsx <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_North_Atlantic_Relative_Index_Sea_Surface_Temp_Student.xlsx>`_
* `North Atlantic Relative Index Sea Surface Temp_noaa_nari_ssta.csv <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_North_Atlantic_Relative_Index_Sea_Surface_Temp_noaa_nari_ssta.csv>`_
* `Sahel Precipitation Anomaly Teacher.xlsx <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_Sahel_Precipitation_Anomaly_Teacher.xlsx>`_
* `Sahel Precipitation Anomaly Student.xlsx <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_Sahel_Precipitation_Anomaly_Student.xlsx>`_
* `Sahel Precipitation Anomaly.csv <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_Sahel_Precipitation_Anomaly.csv>`_
* `Sahel Precipitation Annual Teacher.xlsx <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_Sahel_Precipitation_Annual_Teacher.xlsx>`_
* `Sahel Precipitation Annual Student.xlsx <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_Sahel_Precipitation_Annual_Student.xlsx>`_
* `Sahel Precipitation Annual.csv <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Data/S5_Aerosol_Data/S5_Sahel_Precipitation_Annual.csv>`_

