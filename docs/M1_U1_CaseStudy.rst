Unit A: Case Study, The Sahel
===================================================================

How to Use the Case Study
-----------------------------------------------
The power point has four sections that help provide a context for the importance of studying the Sahel region as a case study for climate change. It also provides a window into the real-life effects that climate change has bestowed upon 100 million people in the region. Leaving them vulnerable to famine, food insecurity, disease, displacement, and force migration. 

The case study also provides opportunities for students to feel empowered by learning how sustainable solutions to some of the pressing problems facing the Sahel can be mitigated. Students engage a design challenge that propels them into studying the problem of lack of clean water and sanitation facing millions not only in the Sahel but around the world. 
 

The editable powerpoint can be downloaded `here <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS_Powerpoint/Sahel_PowerPoint.pptx>`_ (8.7 MB file).

A pdf of the powerpoint, sized for easy download and sharing is also available `here <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS_Powerpoint/Sahel_PowerPoint.pdf>`_ .

Introduction
--------------
The case study of the Sahel is to convey how changes in climate are adversely impacting people in this region. It also highlights efforts to help populations in these regions adapt to these changes by empowering them affordable and sustainable resources to counteract some of the effects of climate variability.

This case study also introduces students to ideas and practices of sustainable design solutions to address global issues such as the lack of clean water. 


Section I. The Sahel
-----------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~

Students become familiar with the geographical region of the Sahel.

Reading
~~~~~~~~~~~~~~~~~~~~
* Climate Change and Variability in the Sahel(United Nations Environment Program), `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS1_The_Sahel/ClimateChangeandVariablityinSahel.pdf>`_

Section II. Defining the Problem
------------------------------------------------------------------------------------------
  
Instructional Goal
~~~~~~~~~~~~~~~~~~~
Help student gain an understanding of the challenges facing the Sahel Region due to the adverse impact of climate variability. 

Learning Objectives
~~~~~~~~~~~~~~~~~~~~
* Describe and identify how the prolonged drought in the Sahel has affected its’ inhabitants. 
* Identify what are the geographic areas of the Sahel that have been affected.
 
Activity
~~~~~~~~~~~~~~~~~~~~
* Sahel Precipitation Patterns, Slide 5 `Word Document <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS2_Defining_The_Problem/CS2_Sahel_Precipitation_Patterns.docx>`_

Section III.  Is The Climate Changing In The Sahel?
---------------------------------------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~
Learn how to identify the signs of climate variability in the Sahel and how the prolonged drought has affected the regions vegetation.

Learning Objectives
~~~~~~~~~~~~~~~~~~~~
* List what time during the year is there increased and reduced precipitation
* Learn how data collected from satellites can document changes in vegetation cover
* Identify the main cause that has affected vegetation growth in the region.

Handout
~~~~~~~~~~~~~~~~~~~~
* African Map, Slide 18 `GIF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS3_Is_Climate_Changing_in_the_Sahel/CS3_African_Map.gif>`_

Readings
~~~~~~~~~~~~~~~~~~~~
* How to Interpret Satellite Images(Earth Observatory, NASA), Slide 19 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS3_Is_Climate_Changing_in_the_Sahel/CS3How_to_Interpret_a_Satellite_Images.pdf>`_, `Online Link <https://earthobservatory.nasa.gov/Features/ColorImage/>`_
* Lake Chad West Africa – Stat Card(United States Geological Survey), Slide 19 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS3_Is_Climate_Changing_in_the_Sahel/CS3LakeChad-WestAfrica.pdf>`_, `Online Link <https://earthshots.usgs.gov/earthshots/sites/all/files/earthshots/LakeChad-WestAfrica.pdf>`_

Activity
~~~~~~~~~~~~~~~~~~~~
* Lake Chad Satellite Image Analysis, Slide 19 `Word Document <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS3_Is_Climate_Changing_in_the_Sahel/CS3Lack_Chad_Satellite_Image_Analysis.docx>`_

Section IV. Mitigation and Sustainable Practices
----------------------------------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~
Learn and describe what are some of the sustainable agricultural practices being implemented by farmers in the Sahel to help mitigate the effects of drought on vegetation and crops.

Learning Objectives
~~~~~~~~~~~~~~~~~~~~
* List specific methods being used by farmers to reduce water runoff.
* List specific methods being used to improve water management.
* Identify how farmers and the people of the Sahel are contribution to reforestation.
* Identify what methods farmers are using to increase crops and yield. 

Activity
~~~~~~~~~~~~~~~~~~~~
* Adapting to Climate Change: Sahel, Slide 20 `Word Document <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS4_Mitigation_and_Sustainable_Practices/CS4_Adaptation_to_Climate_Change_In_the_Sahel.docx>`_

Readings
~~~~~~~~~~~~~~~~~~~~
* Reducing vulnerability to climate change, land degradation and drought (GGWSSI,Great Green Wall for the Sahara and the Sahel Initiative) `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS4_Mitigation_and_Sustainable_Practices/CS4Readings/CS4_Reducing_Vulnerability_to_Climate_Degradation_and_Drought.pdf>`_, `Online Link <http://www.africa-eu-partnership.org/sites/default/files/success-story-files/ggwssi_en_fin.pdf>`_

Section V. Access to Clean Water
----------------------------------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~
Engage students in a sustainable design project that will allow them to view the challenge of access to clean water from a problem-solving perspective.

Learning Objectives
~~~~~~~~~~~~~~~~~~~~
* Conduct research and use information to engage in socially responsible design.
* Think critically, solve problems and use information to develop, design and build a product.
* Communicate idea and collaborate to inform the design process and execute plans.
* Design and build a water filtration prototype.
* Demonstrate creativity and think critically to find viable design solutions, implement and execute them.

Readings
~~~~~~~~~~~~~~~~~~~~
* Water in Africa  (Panda.org), Slide 24 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/CS5%20Readings/CS5waterinafricaeng.pdf>`_, `Online Link <assets.panda.org/downloads/waterinafricaeng.pdf>`_
* Tackling the Global Crisis, International Year of Sanitation (UN Water), Slide 26 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/CS5%20Readings/CS5_IYS_flagship_web_small.pdf>`_, `Online Link <https://esa.un.org/iys/docs/IYS_flagship_web_small.pdf>`_
* Burkina Faso Climate Change (USGS and USAID), `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/CS5%20Readings/CS5_Burkina_Faso_Climate_Change.pdf>`_, `Online Link <https://pubs.usgs.gov/fs/2012/3084/fs2012-3084.pdf>`_
* Niger Climate Change Analysis (USGS and USAID) `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/CS5%20Readings/CS5_Niger_Climate_Change.pdf>`_, `Online Link <https://pubs.usgs.gov/fs/2012/3080/fs2012-3080.pdf>`_
* Chad Climate Change Analysis (USGS and USAID) `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/CS5%20Readings/CS5_Chad_Climat_Change.pdf>`_, `Online Link <https://pubs.usgs.gov/fs/2012/3070/FS2012-3070.pdf>`_

Design Handout Resources
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Engineering Design Process Space Kit 2, Slide 27 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/CS5_Engineering_Design%20_Process%20_Space_Kit_2.pdf>`_, `Online Link <https://sites.google.com/a/mtlsd.net/space-kit-2/ib2/engineering-design-process>`_

Activities and Rubrics
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* IOP GM Curriculum Pilot, Slide 25 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/CS5_IOP-GM-CURRICULUM-PILOT.pdf>`_
* Water Filtration Activity, Slide 25 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/CS5_Waterfiltrationactivity.pdf>`_
* Clean Water Country Research Google Earth Task, Slide 25 `Word Document <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/CS5_Clean_Water_Country_Research_Google_Earth_Task.doc>`_
* Water Filter Grading Scale, Slide 25 `Word Document <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/CS5_Water_Filter_Grading_Scale.docx>`_
* Water Filtration Presentation Guidelines, Slide 25 `Word Document <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/CS5_Water_Filtration_Project_Presentation_Guidlines.docx>`_
* Global Water Projects, Slide 26 `Word Document <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/CS5_Global_Water_Projects.docx>`_
* Water Filtration Materials Brainstorm, Slide 31 `Word Document <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/CS5_Water_Filtration_Materials_Brainstorm.docx>`_

Rubrics
~~~~~~~~~~~~~~~~~~~~
* Presentation Rubric (Buck Institute for Education), Slide 25 `Word Document <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/CS5_Presentation_Rubric_gr9-12.doc>`_, `Online Link <https://www.bie.org/object/document/9_12_presentation_rubric_ccss_aligned>`_
* Power Point Presentation Rubric, Slide 25 `Word Document <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/CS5_PowerPoint_Rubric.doc>`_, `Online Link <http://dms.wcs.k12.va.us/pprubric.htm>`_

Sample water filtration student projects
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Sample 1 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/Studentwork/BB_Presentation.pptx>`_
* Sample 2 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_SahelCaseStudy/CS5_Access_to_Clean_Water/Studentwork/DKC_Presentation.ppt>`_


