Unit B: Data Analysis and Visualization, Paleoclimatology – Tree Rings and Precipitation Data Lab
=======================================================================================================

How To Use This Data Unit
-----------------------------------------------

The Power Point is divided into five sections. The introduction section introduces the field of Paleoclimate and how it used to reconstruct ancient climate. The most important section is the data analysis section. These sections are based on a well-developed Tree Ring Precipitation Data Analysis activity adapted from MY NASA DATA tree ring analysis activity. There are two extensions to this activity - calculating data anomaly and standard deviation, which are two very important statistical measurements in science especially in the evaluation of climate data. There are various files that accompany this lab including. The Power Point will help guide the lab especially the use of Excel and doing statistical computations.

The editable powerpoint can be downloaded `here <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/Clouds_Data_Powerpoint/Clouds_and_Climate_Data.pptx>`_.

A pdf of the powerpoint is also available `here <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/Clouds_Data_Powerpoint/Clouds_and_Climate_Data.pdf>`_.

Introduction
-----------------------------------------------
Paleoclimatology is the study of ancient climates.  Climate ‘proxies’ allow scientists to go back in time and reconstruct the climate conditions over long periods of time - hundreds, thousands even millions of years ago. Climate ‘proxies’ are sources of information from natural archival sources such as: tree rings; ice cores from glaciers and continental ice sheets; corals; sediment cores; and fossilized pollen among others. Climate data can also be obtained from historical records or diaries prior to the 19th Century. 

This data section will give students the opportunity to compare two sets of data – tree rings and precipitation. The will retrieve precipitation data from an open data sources from MY NASA DATA which contains data from various satellite sources. Students will have the opportunity to use Excel as a data analysis and computational tool to produce various graphs. The students will learn how to perform statistical analysis of the data to render it useful. This will give them an opportunity to learn how to use some of the methodologies and techniques used by climatologists and scientist in supporting fields.

Data Section I. Paleoclimate Data 
-----------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Introduce students the field of paleoclimatology and how scientists use climate proxies to reconstruct ancient climate.

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Recognize and describe several paleoclimate proxies and their respective sources.
Explain what are paleoclimate proxies and how they are used.
Gain an understanding of the field of Dendrochrology and learn how to evaluate tree rings to decipher climate data.

Suggested Readings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* What is Paleoclimatology? (NOAA National Center for Environmental Information), Slide 3, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S1_Clouds_Data/S1_What_is_Paleoclimatology_NOAA.pdf>`_, `Online link <https://www.ncdc.noaa.gov/news/what-paleoclimatology>`_

* What are Proxy Data? (NOAA National Center for Environmental Information), Slide 4, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S1_Clouds_Data/S1_What_Are_Proxy_Data_NOAA.pdf>`_, `Online link <https://www.ncdc.noaa.gov/news/what-are-proxy-data>`_

* Paleoclimatology: The Ice Core Record (NASA Earth Observatory), Slide 5, `Online link <https://earthobservatory.nasa.gov/Features/Paleoclimatology_IceCores>`_

* A Record From the Deep Fossil Chemistry (NASA Earth Observatory), Slide 6, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S1_Clouds_Data/S1_Paleoclimatology_A_Record_from_the_Deep.pdf>`_, `Online link <https://earthobservatory.nasa.gov/Features/Paleoclimatology_SedimentCores/paleoclimatology_sediment_cores_2.php>`_

* Picture Climate: How Pollen Tells Us About Climate (NOAA Center for Environmental Information), Slide 7, `Online link <https://www.ncdc.noaa.gov/news/picture-climate-how-pollen-tells-us-about-climate>`_

* 18th Century Ships Logs’ Predict Future Weather Forecasts (Science Daily Article), Slide 8, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S1_Clouds_Data/S1_18th_Century_Ships_Logs_Predict_Future_Weather_Forecast.pdf>`_, `Online link <https://www.sciencedaily.com/releases/2009/10/091006104627.htm>`_

* Studying Tree Rings to Learn About Global Climate (AMNH - American Museum of Natural History), Slide 9, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S1_Clouds_Data/S1_Studying_Tree_Rings_to_Learn_About_Global_Climate.pdf>`_, `Online link <http://www.amnh.org/explore/resource-collections/earth-inside-and-out/studying-tree-rings-to-learn-about-global-climate/>`_

* The Science of Tree Rings, Slide 10, `Online link <http://web.utk.edu/~grissino/index.htm>`_ Note: This website contains many resources for those that wish to expand their knowledge beyond the activities in this unit.

Data Section II. Tree Ring Data Lab
-----------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Learn how to read tree rings to determine age and annual growth based on ring width and access NASA Satellite precipitation data from the region where the tree grew in. 

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Determine the age of a tree by reading the tree rings.
Determine precipitation history of a region by reading tree rings.
Explain what the significance of tree ring variations.
Access precipitation data from MY NASA DATA corresponding to the geographical area of where the tree grew using cardinal coordinates.
Use MY NASA DATA online archive tool to access precipitation data. 

Lab Files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* `Tree Rings (EPA)  <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S2_Clouds_Data/S2_tree_rings_EPA_Lab.pdf>`_

Excel Files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* `Precipitation Boston Data Spreadsheet <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S2_Clouds_Data/S2_Precipitation_Boston_DATA_Spreadsheet.xlsx>`_
* `Precipitation Columbia Data Spreadsheet <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S2_Clouds_Data/S2_Precipitation_Columbia_DATA_Spreadsheet.xlsx>`_
* `Precipitation Jackson Data Spreadsheet <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S2_Clouds_Data/S2_Precipitation_Jackson_DATA_Spreadsheet.xlsx>`_
* `Precipitation Seattle Data Spreadsheet <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S2_Clouds_Data/S2_Precipitation_Seattle_DATA_Spreadsheet.xlsx>`_
* `Tree Ring Precipitation Sample Spreadsheet <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S2_Clouds_Data/S2_Tree_RingPrecipitaiton_SAMPLE_Spreadsheet.xlsx>`_

Data Files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* `Precipitation DATAFILE Boston_MA.txt <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S2_Clouds_Data/S2_Precipitation_DATAFILE_Boston_MA.txt>`_
* `Precipitation DATAFILE Columbia_MO.txt <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S2_Clouds_Data/S2_Precipitation_DATAFILE_Columbia_MO.txt>`_
* `Precipitation DATAFILE Jackson_MS.txt <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S2_Clouds_Data/S2_Precipitation_DATAFILE_Jackson_MS.txt>`_
* `Precipitation DATAFILE Seattle_WA.txt <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S2_Clouds_Data/S2_Precipitation_DATAFILE_Seattle_WA.txt>`_

Suggested Readings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Paleoclimatology: Climate Close-up (Earth Observatory, NASA), Slide 13, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S2_Clouds_Data/S2_Paleoclimatology_Climate_Close_up.pdf>`_, `Online link <https://earthobservatory.nasa.gov/Features/Paleoclimatology_CloseUp/>`_

Data Section III. Importing and Analyzing Data In Excel
----------------------------------------------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Learn how to download satellite precipitation data sets from MY NASA DATA archive to import the data into Excel.   Generate spreadsheets and graphs to facilitate data analysis rendering it useful in order to compare to the tree ring data. 

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Access data using various parameters to access a specific data set corresponding to the tree ring being analyzed.
Download data from MY NASA DATA and import it into Excel.
Use Excel commands and tools to generate spreadsheets using statistical formals to render data ready for graphing.
Generate various types of graphs – column, line, and scattered – to analyze precipitation data and compare it to tree ring data.
Identify seasonal patterns using the data.


Lab Activities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Tree Rings EPA Lab (EPA - Environmental Protection Agency), Slide 11, `Online link <https://www3.epa.gov/climatechange//kids/documents/tree-rings.pdf>`_

Data Section IV. Calculating Anomaly
-----------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Learn how to calculate the anomaly of a precipitation data set by generating spreadsheets and graphs in excel. Gain an understanding of the importance of calculated 

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Calculate averages and normals to generate anomaly data points for decadal data set. 
Use Excel and statistical formulas to generate anomaly data and corresponding graphs. 
Interpret climate anomaly graphs.
Gain an understanding of the value of evaluating data anomalies to learn about climate variability.

Suggested Readings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Averages and Normals (North Caroline State University), `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S4_Clouds_Data/S4_Averages_and_Normals_NC_State_Unv.pdf>`_, `Online link <http://climate.ncsu.edu/edu/k12/.avgandnormals>`_

Data Section V. Calculating Standard Deviation
-----------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Learn how to calculate the standard deviation of a precipitation data set using Excel and gain and understanding of how this calculation lets one  

Learning Outcomes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Calculate the standard deviation for precipitation data using Excel.
Interpret standard deviation in a statistical data set.
Interpret how this measure is useful in analyzing climate data.
Generate a graph using standard deviation calculations.

Lab Activities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Standard Deviation (Lerner.org), Slide 66, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U2_Clouds/Clouds_Data/S5_Clouds_Data/S5_Standard_Deviation_Precipitation.pdf>`_, `Online link <https://www.learner.org/courses/againstallodds/pdfs/AgainstAllOdds_StudentGuide_Unit06.pdf>`_






