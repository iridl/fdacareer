Climate Change and Sustainability Course
===========================================
Developed for Harlem youth at Frederick Douglass Academy (`FDA <http://www.fda1.org>`_).

Supported by the National Science Foundation (`NSF <https://www.nsf.gov>`_) and the International Research Institute for Climate and Society (`IRI <http://iri.columbia.edu>`_).

Introduction
--------------------------------------------------------------------------------------
The idea to develop a course to teach climate change to high school students in Harlem grew out of the integration of research and education called for in proposals to the National Science Foundation’s Faculty Early Career program. The integration sought here hinges on the cause(s) of the period of persistent drought that hit the African Sahel in the 1970s and 1980s – a topic of central interest in the research of the Principal Investigator on the NSF/CAREER grant that funded this endeavor, Alessandra Giannini. The course content was developed by Evelyn Roman-Lazén, a seasoned K-12 educator with a knack for enhancing the teaching of Science, Technology, Engineering and Maths [STEM] disciplines with data-rich experiences for student populations under-represented in STEM, whether in the traditional classroom, in after-school programs or in education and outreach at research institutions. The course is now an Environmental sciences elective at Frederick Douglass Academy [FDA], a public high school in Harlem, in the Science Department led by Pasquale Cusanelli. For the past 4 years, Roman-Lazén has shadowed Jeanne Lynch, a biologist by training and the teacher in charge of the course at FDA. Some of that material is captured here, thanks to Ashley Curtis’ mastery of web editorial technology. Rather than as an exhaustive, all-encompassing resource, we view this website as an opportunity to share a method that we hope may be useful to teachers as they approach this complex issue.

The Sahel is the southern edge of the Sahara – a strip of land a few hundreds of kilometers thick [south to north], and many thousands of kilometers wide [west to east]; from the Atlantic coast of Senegal and Mauritania, to the Red Sea coast of Sudan and Eritrea. The name “Sahel” comes from the Arabic word for shore. The shore that is intended here is that of the archetypal sea of sand, the Sahara Desert. The Sahel is characterized by a very variable climate, starting from the stark alternation of a prolonged dry season, and short, but intense rainy season, during the “monsoon”.

Sulfate aerosols, otherwise known as pollution, are an integral part of the explanation of late-20th century Sahel drought. Closer to home, they were the cause of acid rain, and of a local public health emergency – the asthma epidemic that afflicts communities of color in northern Manhattan. This community now includes a significant minority of students of West African/Sahelian descent and their families. Aerosols provide the entry point to discuss human influence on climate, including unintended consequences. Through aerosols we connect near and far, local and global, just like climate does: one climate for our planet, connecting all of us through variations in the atmosphere and oceans. 

Overall objective 
------------------
Climate change is a complex subject to teach, because it defies scientific disciplinarity. To understand climate and its changing behavior, one needs to know and connect a little bit of everything: physics, chemistry, biology, geology ... On the other hand, climate surrounds us, every moment of our lives. We experience the climate system on a daily basis when we ask ourselves: will it rain today? Where do these clouds come from? Why is it so hot/cold?

This course is intended to provide a guided introduction to some basic concepts from the perspective of “climate justice”. It has a strong science base, but also explores sustainable solutions. It is not intended to be exhaustive of the topic by any stretch of the imagination, rather to provide a method. Concepts are broken down into their science constituents in the lecture slides, with the online outline providing a guide to step through them. The learning experience is enriched with data analysis activities, and case studies for further discussion. All material is research-grade. The data was obtained from reputable sources, most notably U.S. government agencies such as the National Aeronautics and Space Administration [NASA], the National Oceanic and Atmospheric Administration [NOAA] and the Environmental Protection Agency [EPA]. The relevant articles collected and referenced come from the peer-reviewed literature.

Audience
---------
At FDA, the typical student population taking this course has included students in their junior or senior year of high school. Since a goal of the course is to build on and integrate prior knowledge in the basic scientific disciplines [physics, chemistry, biology...], basic scientific concepts may be considered as a pre-requisite to taking this course. However, the motivation and approach can also be flipped: this course can serve as the starting point to teach concepts in the basic scientific disciplines [physics, chemistry, biology...], based on their relevance to understanding how Earth’s climate system works.

How the curriculum works
-------------------------

Evelyn Roman-Lazén and Jeanne Lynch developed and implemented the curriculum over the course of 4 years. They worked collaboratively to create an interdisciplinary learning environment full of opportunities for students to engage in Socratic discourse and experiential learning. The pedagogical tools shared here [lecture materials, multi-media resources including readings, data analysis activities, and case study discussions] were used to engage students in conversations that stimulated critical thinking, and elicited their knowledge and ideas, as well as their underlying presumptions. Students were continuously encouraged to ask and answer questions and to engage in argumentative, cooperative dialogue about issues related to climate science, environmental justice, policy and sustainable solutions. The classroom environment fomented immersive learning experiences as well as ample opportunities for students to reflect, design, build and contribute to the transformation of the environment in and out of school. 

Course contributors
---------------------

`Evelyn Roman-Lazén <evelyn.roman.lazen@gmail.com>`_, Course and Curriculum Developer and Co-Instructor, Educational Consultant 

`Alessandra Giannini <https://iri.columbia.edu/contact/staff-directory/alessandra-giannini/>`_, Research Scientist at the `International Research Institute for Climate and Society <http://iri.columbia.edu>`_ 

`Ashley Curtis <https://iri.columbia.edu/contact/staff-directory/ashley-curtis/>`_, Education Technologist, Senior Staff Associate at the `International Research Institute for Climate and Society <http://iri.columbia.edu>`_ 

Course Content
---------------
The course is composed of four units: aerosols, clouds, global warming and atmosphere. 
Each unit includes an online outline, which is matched by a powerpoint file with classroom lecture slides, data activities and a case study. A teacher can follow the outline to discover linked resources, or can browse all available resources, which are organized by unit. 

Content is available by browsing units via the links below. Alternatively, all materials are available `here  <https://fdacourse.iri.columbia.edu/documents/>`_ for quick access.  All content and materials can also be accessed through the menu at the top of the page.

**Atmosphere Module**

.. toctree::
   :maxdepth: 1

   Unit A: Atmospheric Aerosols and Climate Change <M1_U1_Aerosols>
   Unit A: Data Analysis and Visualization, Atmospheric Aerosols and Climate Change <M1_U1_Data>
   Unit A: Case Study, The Sahel <M1_U1_CaseStudy> 
   Unit B: Clouds and Climate Change <M1_U2_Clouds>
   Unit B: Data Analysis and Visualization, Paleoclimatology <M1_U2_Data>
   Unit C: Global Warning <M1_U3_Global_Warming> 
   Unit D: Atmosphere and Climate Change <M1_U4_Circulation>
   
How to approach this material
-----------------------------------------------
Each unit includes an online outline, which is matched by a powerpoint file with classroom lecture slides, data activities and a case study. A teacher can follow the outline to discover linked resources, or can browse all available resources, which are organized by unit.   

Each of the units can be used independently from one another, sequentially or not. It all depends on preference in how to introduce the topic at hand and what tool is deemed most appropriate. 

The main Power Point dives right into the science in an interdisciplinary fashion. This tool is designed to help demystify the content and to enhance understanding. 

The Data Analysis Tool is designed to engage students in learning how to use Excel to analyze and represent scientific data used by scientists to study climate change phenomena. The data tools have step-by-step instructions on how to access data and process it. It also helps teach how to perform statistical analysis to render the data useful. 

The third tool is a Case Study that helps introduce the impact these climatological changes are having on people and ecosystems. In the Sahel Case Study students also have the opportunity to learn about sustainable solutions by engaging in a engineering design project.

Each Power Point has robust notes on most slides that serve as a lesson plan of sorts. All the pertinent information and downloadable teaching tools are also organized in a sequential fashion in the note section of the slides. The Power Points have downloaded videos to avoid lag time, access and streaming problems. In the note sections there are also the links to all related images and downloaded documents in case needed.
Each tool includes:

* Student assignments in the from of handouts, 
* Lab activities with step by step instructions and analysis questions; 
* Video transcripts when appropriate;
* Suggested Reading assignments in PDF format


Acknowledgements
------------------------------------------------------------------------------------------
We would like to thank Jeanne Lynch, Biology, Environmental and Sustainability Teacher at Frederick Douglass Academy High School and Pasquale Cusanelli, Assistant Principal and Science Department Chair at Frederick Douglass Academy High School for their contributions to this course.  

