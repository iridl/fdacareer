Unit A: Atmospheric Aerosols and Climate Change
===================================================================

How to approach this material
-----------------------------------------------
Each unit includes an online outline, which is matched by a powerpoint file with classroom lecture slides, data activities and a case study. A teacher can follow the outline to discover linked resources, or can browse all available resources, which are organized by unit.   

The editable powerpoint can be downloaded `here <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_PowerPoints/Aerosols_large.pptx>`_ (warning: large 1.5G file).

A pdf of the powerpoint, sized for easy download and sharing is also available `here <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_PowerPoints/Aerosols_small.pdf>`_

Each of the units can be used independently from one another, sequentially or not. It all depends on preference in how to introduce the topic at hand and what tool is deemed most appropriate. 

The main Power Point dives right into the science in an interdisciplinary fashion. This tool is designed to help demystify the content and to enhance understanding. 

The Data Analysis Tool is designed to engage students in learning how to use Excel to analyze and represent scientific data used by scientists to study climate change phenomena. The data tools have step-by-step instructions on how to access data and process it. It also helps teach how to perform statistical analysis to render the data useful. 

The third tool is a Case Study that helps introduce the impact these climatological changes are having on people and ecosystems. In the Sahel Case Study students also have the opportunity to learn about sustainable solutions by engaging in a engineering design project.

Each Power Point has robust notes on most slides that serve as a lesson plan of sorts. All the pertinent information and downloadable teaching tools are also organized in a sequential fashion in the note section of the slides. The Power Points have downloaded videos to avoid lag time, access and streaming problems. In the note sections there are also the links to all related images and downloaded documents in case needed.
Each tool includes:

* Student assignments in the from of handouts, 
* Lab activities with step by step instructions and analysis questions; 
* Video transcripts when appropriate;
* Suggested Reading assignments in PDF format


Introduction
--------------
Aerosols are tiny solid particles and minuscule liquid droplets suspended in the lower atmosphere. There are two types of aerosols, those that occur naturally and others that are created due to human activity.  

Aerosols individual chemical characteristics define how these particles interact with water vapor and radiation in the lower atmosphere. Aerosols are essential to the formation of clouds. In order for clouds to form the tiny water vapor droplets need a surface to attach to. The minute specs of aerosols suspended in the lower atmosphere provide such a surface. Once the water vapor attaches it will change from its gas form to liquid water.  Aerosols provide a surface where microscopic water vapor droplets can attach and change into liquid through the physical process of condensation. The aerosols involved in the process of cloud formation are called cloud condensation nuclei (CCN). In addition of the presence of CCN’s for clouds the form condensation also needs to occur. This happens when water vapor meets cooler air throughout the earth’s troposphere.

Section I. Aerosols: What are they?
-----------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~~

Introduce students to the physical and chemical properties of some of the most common aerosols.

Learning Outcomes
~~~~~~~~~~~~~~~~~~~

* Use inquiry skills to observe and describe some of the properties of common aerosols – color, shape and luster. 
* Understanding what is the nature of aerosols and their sources.
* Differentiate between natural aerosols and anthropogenic aerosols.
* Describe how aerosols interact with sunlight by scattering and absorbing it.
* Describe what are the properties mostly common with aerosols that scatter and absorb light.
* Describe the how aerosols have a cooling or heating effect on the atmosphere.

Lab Activities
~~~~~~~~~~~~~~~~~~~~
* Physical Properties of Solid Aerosols Student Activity, Slide 1, `Word Document <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Tasks_labs/S1_Physical_Properties_of_Solid_Aerosols.docx>`_
* What are aerosols?, Slide 2, `Word Document <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Tasks_labs/S1_Task_What_are_Aerosols.docx>`_
* Aerosols and Incoming Sunlight, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Tasks_labs/S1_Task_Aerosols_and_Incoming_Sun_Light.pdf>`_

Suggested Readings
~~~~~~~~~~~~~~~~~~~~
* The Basics, Aerosols: Tiny Particles, Big Impact Reading, Slide 2, `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Readings/S1_Aerosol_Readings/S1_The_Basics_Aerosols_Tiny_Particles_Big_Impact%20.pdf>`_
* Aerosols and Incoming Sunlight (Earth Observatory, NASA), `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Readings/S1_Aerosol_Readings/S1_Aerosols_and_Incoming_Sun_Light.pdf>`_, `Online Link <https://earthobservatory.nasa.gov/Features/Aerosols/>`_

Section II. Where do Aerosols Come From?
------------------------------------------------------------------------------------------
  
Instructional Goal
~~~~~~~~~~~~~~~~~~~
Introduce students to the different sources of Aerosols. 

Learning Objectives
~~~~~~~~~~~~~~~~~~~~
* Describe and understand what are the processes that produce natural and anthropogenic aerosols.
* Describe which are the sources of natural and anthropogenic aerosols and which are the most common and abundant.
* Describe what types of emissions produce the most anthropogenic aerosols, and which ones. 

Suggested Readings
~~~~~~~~~~~~~~~~~~~~
* From Dust Bowl to The Sahel (Earth Observatory, NASA), Slide 12 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Readings/S2_Aerosol_Readings/S2_From_the_Dust_Bowl_to_the_Sahel.pdf>`_, `Online Link <https://earthobservatory.nasa.gov/Features/DustBowl/printall.php/>`_

Section III.  Deforestation and Aerosols
---------------------------------------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~
Gain and understanding that deforestation can increase the level of aerosols in the atmosphere.

Learning Objectives
~~~~~~~~~~~~~~~~~~~~
* Describe how deforestation contributes to the increase in aerosols and of what type.
* Locate what areas of the Amazon Forest have been deforested using Google Earth.
* Calculate the rate of change of deforestation for each decade of the Rodonia, Brazil. 
* Analyze and evaluate the rate of change of deforestation in Rodonia, Brazil.

Suggested Readings
~~~~~~~~~~~~~~~~~~~~
* Amazon Deforestation (Earth Observatory, NASA), Slide 17 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Readings/S3_Aerosol_Readings/S3_Amazon_Deforestation_NASA.pdf>`_, `Online Link <http://earthobservatory.nasa.gov/Features/WorldOfChange/deforestation.php>`_

Section IV. Atmospheric Aerosols
----------------------------------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~
Gain and understanding how aerosols from the Sahara Desert are dispersed across the globe. Explain how sand from the Sahara desert is travels across the globe. 

Learning Objectives
~~~~~~~~~~~~~~~~~~~~
* Describe how the aerosols from the Sahara desert contribute to the ecology of the Amazon.
* Understand what mechanisms contribute to the aerosols from the Sahara Desert reach across the globe.
* Describe what technological tools are used to record data about the dispersal of aerosols. 

Lab Activities
~~~~~~~~~~~~~~~~~~~~
* Aerosol Patterns, Slide 22 `Word Document <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Tasks_labs/S4_Task_Aerosol_Patterns.docx>`_

Suggested Readings
~~~~~~~~~~~~~~~~~~~~
* Patterns: Patterns - Aerosols: Tiny Particles, Big Impact (Earth Observatory, NASA), Slide 22 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Readings/S4_Aerosol_Readings/S4_Patterns_Aerosols_Tinly_Particles_Big_Impact.pdf>`_, `Online Link <https://earthobservatory.nasa.gov/Features/Aerosols/page2.php>`_
* Transcript Video Sahara Dust to Amazon (Goddard Media Studios, NASA), Slide 25 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Readings/S3_Aerosol_Readings/S3_Amazon_Deforestation_NASA.pdf>`_, `Online Link <https://svs.gsfc.nasa.gov/vis/a010000/a011700/a011775/script_21076_00.html>`_
* CALIPSO Cloud Aerosol Satellite Observations (NASA Facts), Slide 25 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Readings/S4_Aerosol_Readings/S4_Calipso_Cloud%20Aerosol_Satellite_Observations.pdf>`_, `Online Link <https://www.nasa.gov/pdf/137028main_FS-2005-09-120-LaRC.pdf>`_

Section V. Measuring Aerosols
----------------------------------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~
Learn to read and analyze visualizations of aerosol data using the optical depth scale.

Learning Objectives
~~~~~~~~~~~~~~~~~~~~
* Describe how aerosols are measured.
* Use the optical depth scale to analyze aerosol data visualizations.
* Explain what scale is used in the measurement of aerosols and what it means.
* Describe patterns in the geological distribution of aerosols across the globe using data visualization images.

Lab Activities
~~~~~~~~~~~~~~~~~~~~
* Measuring Aerosols, Slide 25 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Tasks_labs/S5_Task_Measuring_Aerosols.pdf>`_

Suggested Readings
~~~~~~~~~~~~~~~~~~~~
* Measuring Aerosols: Tiny Particles, Big Impact (Earth Observatory, NASA), Slide 26 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Readings/S5_Aerosol_Readings/S5_Measuring_Aerosols_%20Tiny%20Particles,%20Big_Impact.pdf>`_, `Online Link <https://earthobservatory.nasa.gov/Features/Aerosols/page5.php>`_
* Aerosol Optical Depth Video Description (NASA Earth Observatory), Slide 30 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Readings/S5_Aerosol_Readings/S5_Aerosol_Optical_Depth_Video_Description.pdf>`_, `Online Link <http://earthobservatory.nasa.gov/GlobalMaps/view.php?d1=MODAL2_M_AER_OD>`_

Section VI. Analyzing Visual Data
----------------------------------------------------------------------------------

Instructional Goal
~~~~~~~~~~~~~~~~~~~
Learn how to analyze satellite images to study aerosols and what technological tools are used to collect data and images.

Learning Objectives
~~~~~~~~~~~~~~~~~~~~
* Use satellite images of an erupting volcano to decipher the size and direction of the plume of volcanic smoke. 
* Explain the importance of the use of these tools to understand aerosol volcanic emissions.
* Describe how false color images are used to highlight and identify features.
* Explain how false color visual animation of aerosol global atmospheric motions are used and what information they can convey.
* Explain what type of data is used to create the motion of aerosol atmospheric models.
* Explain what is the value of this type of data collection and what it can be used for. 

Suggested Readings
~~~~~~~~~~~~~~~~~~~~
* MODIS Brochure – Read section Our Changing Atmosphere (NASA), Slide 31 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Readings/S6_Aerosol_Readings/S6_modis_brochure.pdf>`_, `Online Link <hhttps://modis.gsfc.nasa.gov/about/media/modis_brochure.pdf>`_
* Chaitén (Chaiten Volcano) – Chile (Geology.com), Slide 32 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Readings/S6_Aerosol_Readings/S6_Chait%c3%a9n_Volcano_Chile.pdf>`_, `Online Link <http://geology.com/volcanoes/chaiten/>`_
* USGS Volcano Hazards (United States Geological Survey), Slide 32 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Readings/S6_Aerosol_Readings/S6_USGS_Volcano_Hazards_Program.pdf>`_, `Online Link <https://volcanoes.usgs.gov/vhp/gas.html>`_
* Volcanoes and Climate Change (NASA Earth Observatory), Slide 35 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Readings/S6_Aerosol_Readings/S6_Volcanoes_and_Climate-Change.pdf>`_, `Online Link <http://earthobservatory.nasa.gov/features/volcanoes>`_
* Description Video Aerosol Transport and Assimilation (NASA), Slide 37 `PDF <https://fdacourse.iri.columbia.edu/documents/M1U1_Aerosols/Aerosol_Readings/S6_Aerosol_Readings/S6_Description_Aerosol_Transport_and_Assimilation.pdf>`_, `Online Link <https://gmao.gsfc.nasa.gov/research/aerosol/>`_

